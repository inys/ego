package config

import (
	"encoding/json"
	"os"
)

type conf interface {
	SrvName() string
}

var cf conf

func InitConf(v conf) error {

	file, err := os.ReadFile(os.Getenv("CONF"))
	if err != nil {
		return err
	}

	err = json.Unmarshal(file, v)
	if err != nil {
		return err
	}

	cf = v

	return nil
}

func SrvName() string {
	return cf.SrvName()
}
